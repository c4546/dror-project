# Dror work - Man In The Middle Arp Attack using Scapy
# version 2.2
import re
import fire
from mitm import man_in_the_middle
from sys import argv

'''
importing fire module in order to expose the full contents of the program to the command line and to be
able to run the script from the terminal with only one command.
import re module in order to use regular expressions functions.
importing argv so I will be able to check if the user wants help in the terminal.
'''

"""
This function Triggers a man in the middle attack using the function man_in_the_middle()
it's arguments are; ip: target ip address, r: router ip address, interface: interface to operate on.
if the arguments were validated by validator() it execute the attack, if not it raise an error.
"""


def attack(target_ip, router_ip, interface):
    if validator(target_ip) and validator(router_ip):
        man_in_the_middle(target_ip, router_ip, interface)
    else:
        raise ValueError('arguments error, target ip or router ip not follows any ip pattern\n',
                         'given: ip=' + target_ip,
                         ' r=' + router_ip)


"""
This function checks if a given ip address is following any ip pattern using Regex.
It returns true if the ip is valid otherwise return false.
"""


def validator(target_ip):
    if re.match(r"^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$", target_ip) is not None:
        return True
    return False


'''
this condition will check if the code is being imported or being executed 
and runs it only if it was executed, to prevents mistakes.
'''


if __name__ == '__main__':
    if argv[1] == '-h':
        # help menu
        print('\n~~~~~~~~~~~~~~~~~~~~ Help ~~~~~~~~~~~~~~~~~~~~\n')
        print('Usage: sudo python main.py target-ip router-ip interface')
        print('e.g    sudo python main.py 192.168.1.15 192.168.1.1 en0')
        print('\n~~~~~~~~~~~~~~~~~~~~ End Help ~~~~~~~~~~~~~~~~~~~~\n')
    else:
        # for users who knows exactly what they want
        fire.Fire()
